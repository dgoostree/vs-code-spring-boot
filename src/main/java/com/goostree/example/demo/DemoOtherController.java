package com.goostree.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DemoOtherController {

        @RequestMapping("/earth")
        public String earth() {
            
            return "/test.html";
        }
}